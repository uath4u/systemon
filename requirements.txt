Django==3.2.6
djangorestframework==3.12.4
requests==2.26.0
docker==5.0.0
six==1.16.0
psutil==5.8.0
