from pytest import fixture

from systemon.db.handler.client_process import ClientConfigDbHandler
from systemon.connector.stm_sqlite3 import connect_to_db
from systemon.db.handler.ip_hostname import IpHostnameHandler
from systemon.db.handler.smtp_email import EmailHandler
from systemon.settings import DB_FILE


@fixture(scope='module', autouse=True)
def clean_database():
    try:
        db_connection, db_cursor = connect_to_db()
        db_tables = db_cursor.execute("SELECT name FROM sqlite_master WHERE "
                                      "type ='table' AND "
                                      "name NOT LIKE 'sqlite_%'").fetchall()
        for db_table in db_tables:
            query = f"DELETE FROM {db_table['name']}"
            db_cursor.execute(query,)
            db_connection.commit()
    finally:
        db_connection.close()


def test_client_process(clean_database):
    with ClientConfigDbHandler() as handler:
        handler.get_all_processes()
        handler.post_processes(hostname='test', process='test')
        handler.put_processes(new_hostname='Test', hostname='test',
                              process='test', new_process='test')
        handler.put_processes(process='test', hostname='Test',
                              new_process='test')
        handler.put_processes(process='test', hostname='Test',
                              new_hostname='test')
        handler.get_host_config(hostname='test')

def test_ip_hostname(clean_database):
    with IpHostnameHandler() as handler:
        handler.post_host('blub')
        handler.get_spec_host('blub')
        handler.put_host(hostname='blub', new_hostname='test_ip_handler')

def test_email(clean_database):
    with EmailHandler() as handler:
        handler.post_email(email_type='test', text='test {test}',
                           subject='test email')
        handler.get_spec_email()
        handler.put_email(email_type='test', new_text='test 2 changed',
                          new_subject='test email 2', new_type='test')
