from systemon.notifier.smtp_handler import build_from_db, send_email

def test_send_email():
    email_to_send = build_from_db(email_to='lars.schwarzer@ls-engineer.de')
    print(email_to_send)
    send_email(email_to_send=email_to_send)
