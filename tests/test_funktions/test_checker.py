import sqlite3
from pytest import fixture

from systemon.checker.ch_ip_ping import ping_ip
from systemon.checker.ch_process import find_proc
from systemon.checker.ch_docker_container import container_status
from systemon.connector.stm_docker_container import connect_to_container
from systemon.settings import DB_FILE

def test_print_ip():
    ping_ip()

def test_find_proc():
    find_proc()

def test_ch_container():
    try:
        docker_client, docker_container = connect_to_container('keen_yonath')
        if docker_container and docker_container.status != 'running':
            docker_container.start()

        assert container_status('blub') == 1
        assert container_status('keen_yonath') == 'running'

    finally:
        docker_container.stop()
        docker_client.close()
