#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""Handler classes for clients"""

from threading import Thread, Event

from components.connectors import connect_to_container
from components.connectors import find_process


class HandleDocker(Thread):
    """Handles stropped processes"""

    def __init__(self, docker_name: str, shutdown: Event):
        """Init function for HandlesStoppedProc"""

        super().__init__(name=docker_name)
        self.shutdown = shutdown
        self.docker_name = docker_name

    def run(self):
        """Handle one stopped docker"""

        try:
            client, container = connect_to_container(self.docker_name)
            handler_status = True

            while not self.shutdown.is_set():

                container = client.containers.get(self.docker_name)

                if container.status == 'running' and handler_status == False:
                    print(f'{container} is back again')
                    handler_status = True
                elif container.status != 'running' and handler_status == True:
                    print(f'{container} is stoped first time')
                    handler_status = False

        finally:
            if client:
                client.close()
            print('found new docker')
            return 0


class HandleProcess(Thread):
    """Handles stropped processes"""

    def __init__(self, process_name: str, shutdown: Event):
        """Init function for HandlesStoppedProc"""

        super().__init__(name=process_name)
        self.shutdown = shutdown
        self.process_name = process_name

    def run(self):
        """Handle one stopped process"""

        process = find_process(self.process_name)
        handler_status = True

        if not process:
            print(f'{self.process_name} is down')
            handler_status = False

        while not self.shutdown.is_set():

            while handler_status == False:
                process = find_process(self.process_name)
                if process:
                    print(f'{self.process_name} is up again')
                    handler_status = True

            if not process.is_running() and handler_status == True:
                print(f'{self.process_name} is down first time')
                handler_status = False

        return 0
