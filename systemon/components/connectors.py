#!/usr/bin/env python
#-*- conding: utf-8 -*-
"""Connector functions for systemon"""

from requests.exceptions import HTTPError
from docker import client
import psutil
from smtplib import SMTP_SSL
from components.comp_settings import SMTP_SERVER_ADDRESS, SMTP_CREDENTIALS


def connect_to_container(id_or_name):
    """Connect to docker and returns a container by name or id"""

    try:
        docker_client = client.from_env()
        docker_container = docker_client.containers.get(id_or_name)
        return docker_client, docker_container

    except HTTPError:
        if docker_client:
            return docker_client, None
        else:
            return None, None

def find_process(process_name):
    """Search the process by name"""

    process = None

    for proc in [p for p in psutil.process_iter() if
                 p.name() == process_name]:

        if not process:
            process = proc

        elif len(proc.parents()) < len(process.parents()):
            process = proc

    if process:
        return process
    else:
        return None

class StmSMTPSSL(SMTP_SSL):
    """Class containing SMTP connection with config for systemon"""

    def __init__(self):
        """Init method"""

        super().__init__(host=SMTP_SERVER_ADDRESS[0],
                         port=SMTP_SERVER_ADDRESS[1])

        self.login()

    def login(self):
        """Connect to smtp server"""

        super().login(user=SMTP_CREDENTIALS[0],
                      password=SMTP_CREDENTIALS[1])

if __name__ == '__main__':

    with StmSMTPSSL() as SmtpClient:
        SmtpClient.login()
        print(SmtpClient.noop())
