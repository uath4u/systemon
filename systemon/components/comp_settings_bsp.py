from pathlib import Path


# Email Configuration

IMAP_SERVER_ADDRESS = 'address'#!/usr/bin/env python3
#-*- coding: utf-8 -*-
"""Set up shared variables"""

# Setup pathes

CWD = Path.cwd() / 'controler'

CERT_FILE = CWD / 'cert' / 'cert.pem'
KEY_FILE = CWD / 'cert' / 'key.pem'

# Setup Server and Client variables

SERVER_ADDRESS = ('127.0.0.1', 8000)

# Email Configuration

SMTP_SERVER_ADDRESS = ('Server_address', '465')
SMTP_CREDENTIALS = ('USER_NAME', 'PASSWORD')
SMTP_EMAIL_ADDRESS = 'EMAIL_ADDRESS'

# REST-API Crendentials

REST_API_ADDRESS = 'http://localhost:8000/rest-api/'
REST_USERNAME = 'REST USERNAME'
REST_PASSWORD = 'REST PASSWORD'
