#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""Starts and controlls the client"""

import requests
import socket
import json
import sys
from threading import Event

from components.handlers import HandleDocker, HandleProcess
from components.comp_settings import REST_USERNAME, REST_PASSWORD

class ClientControler():
    """Starts and controlls the client"""

    def __init__(self):
        """Initfunction for ClientControler"""

        self.hostname = socket.gethostname()
        self.shutdown = Event()
        self.ip = self.get_ip()
        self.all_running_threads = []
        self.header = self.build_header()
        self.client_processes, self.client_dockers = self.get_config()
        self.start_checker()

    def build_header(self):
        """Build the header for requests"""
        return {'Authorization': f'token {self.get_auth_token()}'}

    def get_auth_token(self):
        """Get the token to authenticate to the REST-API"""
        return requests.post('http://localhost:8000/rest-api/login',
                             data={'username': REST_USERNAME,
                             'password': REST_PASSWORD}).json()['token']

    def get_config(self):
        """Gets config from REST API"""

        response = requests.post('http://localhost:8000/rest-api/client-config',
                                 data={'hostname': self.hostname,
                                       'ip': self.ip},
                                 headers=self.header).json()

        return response['client_processes'], response['client_dockers']


    def get_ip(self):
        """Gets own IP address"""
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(('10.255.255.255', 1))
            ip = s.getsockname()[0]
        except Exception:
            ip = '127.0.0.1'
        finally:
            s.close()
        return ip

    def start_checker(self):
        """Starts all necessary checker"""

        for docker_to_check in self.client_dockers:
            thread = HandleDocker(docker_to_check['docker_to_check'],
                                  self.shutdown)
            thread.start()
            self.all_running_threads.append(thread)

        for process_to_check in self.client_processes:
            thread = HandleProcess(process_to_check['process_to_check'],
                                   self.shutdown)
            thread.start()
            self.all_running_threads.append(thread)

    def thread_supervisor(self):
        """Supervise all running threads"""

        i = 0

        while not self.shutdown.is_set():
            i += 1
            for thread in self.all_running_threads:
                if not thread.is_alive():
                    print(f'{thread} has died!!!')
            print(self.all_running_threads)

            if i == 5:
                i = 0
                blub = input('Should i stop now?')
                if blub == 'y':
                    self.shutdown.set()

        for thread in self.all_running_threads:
            thread.join()
            print(thread)

        sys.exit(0)


if __name__ == '__main__':
    obj = ClientControler()
    obj.thread_supervisor()
    # obj.get_auth_token()
