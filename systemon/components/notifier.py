#!/ust/bin/env pyth
#-*- coding: utf-8 -*-
"""Contains notifier for systemon"""

from email.parser import BytesParser
from smtplib import SMTPConnectError
from components.connectors import StmSMTPSSL
from components.comp_settings import SMTP_EMAIL_ADDRESS


class EmailNotifier():

    def __init__(self, email_to: str, email_text_bytes: bytes, subject: str):
        """Init function for EmailNotifier"""

        self.email_to = email_to
        self.email_text_bytes = email_text_bytes
        self.subject = subject
        self.email = build_email()

    def build_from_db(self):
        """Build and returns the email from bytes"""

        email_to_send = BytesParser().parsebytes(self.email_text_bytes.encode())
        email_to_send['Subject'] = self.subject
        email_to_send['From'] = SMTP_EMAIL_ADDRESS
        email_to_send['To'] = self.email_to
        return email_to_send

    def send_email(self):
        """sends email form default smtp server to recipient"""
        try:
            with StmSMTPSSL() as smtp_connection:
                smtp_connection.send_message(self.email)
            return 0
        except SMTPConnectError:
            return 1
        else:
            return 1
