
#!/usr/bin/env python3
#-*- utf-8 -*-
"""Ping a given ip address"""

from subprocess import check_output, CalledProcessError


def ping_ip(ip='localhost'):
    """Ping a given ip address"""

    try:
        check_output(['ping', '-c', '1', ip])
        return True

    except CalledProcessError:
        return False
