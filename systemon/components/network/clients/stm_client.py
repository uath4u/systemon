#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""Here some text"""

import socket
import sys
import ssl

from time import sleep
from pathlib import Path
from systemon.checker.process import find_proc
from systemon.settings import CERT_FILE, KEY_FILE, SERVER_ADDRESS


def build_client() -> socket.socket:
    """client to send some data"""

    ssl_context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=
                                             CERT_FILE)

    ssl_context.load_cert_chain(certfile=CERT_FILE,
                                keyfile=KEY_FILE,
                                password='test123')
    client_socket = socket.socket(family=socket.AF_INET)
    ssl_client_socket = ssl_context.wrap_socket(client_socket,
                                                server_hostname='localhost')

    print('Client socket was build and ssl wrapped')

    try:
        ssl_client_socket.connect(SERVER_ADDRESS)
    except ConnectionRefusedError:
        print(f'Server on {SERVER_ADDRESS} does not respond')
        sys.exit(1)

    print('Client socket connected to server')
    print(f'SSl protocol is {ssl_client_socket.version()}')
    return ssl_client_socket

def handle_data(client_socket: socket.socket):
    """sends test data to server"""

    response = True

    try:
        while response:

            sleep(2)

            if (response := client_socket.recv(4096)):
                print(response)
            else:
                response = False
                break

            if (data_to_send := find_proc(name='chrome')):
                print(f'{data_to_send.name()}, {data_to_send.pid}')
                client_socket.send(f'Process {data_to_send.name()} is alive '
                                   f'with pid {data_to_send.pid}'.encode())
            else:
                client_socket.send(f'Process is down'.encode())

    except KeyboardInterrupt:
        pass

    finally:
        if response:
            print('\nclient connection was closed by client')
            client_socket.shutdown(socket.SHUT_RDWR)
        else:
            print('Client connection was closed by server')

        client_socket.close()

def get_data(client_socket: socket.socket):
    """get data back from server"""

    data = client_socket.recv(4096)
    print(f'The data received is {data}')

    return data.decode('utf-8')


if __name__ == '__main__':

    handle_data(build_client())
    sys.exit(0)
