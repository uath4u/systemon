#!/usr/bin/env python3
# -*- coding: utf-8 -*
"""Class to set up a socket server to connect to clients and handle there input
"""

import socket
import sys
import ssl

from pathlib import Path
from threading import Thread, Event
from components.comp_settings import SERVER_ADDRESS, CERT_FILE, KEY_FILE


class Server(Thread):

    def __init__(self, shutdown: Event):
        """Init method"""

        super().__init__(name='Server')
        self.SERVER_ADDRESS = (SERVER_ADDRESS)
        self.shutdown = shutdown

    @classmethod
    def build_server_socket(cls):
        """Build the server socket"""

        cls.__init__(cls)
        cls.server_socket = socket.create_server(address=SERVER_ADDRESS,
                                                 reuse_port=True,
                                                 family=socket.AF_INET)
        cls.server_socket.listen(10)
        print('server_socket was build')
        return cls()

    def run(self):
        """Establish new client connections and starts new Thread to handle the
        connection"""

        ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH,
                                                 cafile=CERT_FILE)
        ssl_context.load_cert_chain(certfile=CERT_FILE,
                                    keyfile=KEY_FILE,
                                    password='test123')

        try:
            while not self.shutdown.is_set():
                connection, connection_address = self.server_socket.accept()
                ssl_connection = ssl_context.wrap_socket(connection,
                                                         server_side=True)
                Thread(target=self.handle_connection, args=(ssl_connection,
                connection_address)).start()

        except KeyboardInterrupt:
            self.server_is_up = False

        finally:
            self.server_socket.shutdown(socket.SHUT_RDWR)
            self.server_socket.close()
        sys.exit(0)

    def handle_connection(self, client_connection, connection_address):
        """Receives und handle the data for one client"""

        try:
            client_connection.send(f'{self.server_is_up}'.encode())
            data = client_connection.recv(4096)
            print(f'Received data from {connection_address}:\n{data}')

            if data:
                print(f'Connection {connection_address} was closed by server')
                client_connection.shutdown(socket.SHUT_RDWR)
            else:
                print(f'Connection {connection_address} was closed by client')

        finally:
            client_connection.close()


if __name__ == '__main__':

    server = Server.build_server_socket()
    server.start_server()
    sys.exit(0)
