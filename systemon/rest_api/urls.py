from django.urls import path, include

from rest_framework.routers import DefaultRouter

from rest_api import views

router = DefaultRouter()
router.register('profile', views.UserProfileViewSet)

urlpatterns = [
    path('client-config', views.ClientConfig.as_view()),
    path('login', views.UserLoginApiView.as_view()),
    path('', include(router.urls)),
    path('token-is-valid', views.TokenIsValid.as_view()),
    path('report', views.HandleComponents.as_view()),
]
