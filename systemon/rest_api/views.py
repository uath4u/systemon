from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework import filters
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.permissions import IsAuthenticated, AllowAny

from rest_api import models
from rest_api import serializers
from rest_api import permissions


class ClientConfig(APIView):
    """Manage the config for a client"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    http_method_names = ('post',)

    def post(self, request, format=None):
        """Returns the config for a client"""

        client_processes = []
        client_dockers = []

        for entry in request.data.values():

            found_processes = serializers.ClientProcsModelSerializer(
                                models.ClientProcesses.objects.filter(
                                    ip_or_hostname__ip_or_hostname__exact=
                                    entry),
                                many=True).data

            found_dockers = serializers.ClientDockerModelSerializer(
                                models.ClientDockers.objects.filter(
                                    ip_or_hostname__ip_or_hostname__exact=
                                    entry),
                                many=True).data

            client_dockers += found_dockers
            client_processes += found_processes

        response_data = {'client_processes': client_processes,
                         'client_dockers': client_dockers}

        return Response(data=response_data,
                        headers= {'Content-Type': 'application/json'})


class TokenIsValid(APIView):
    """Checks if a token is valid"""

    authentication_classes = (TokenAuthentication,)
    http_method_names = ('post',)

    def post(self, request, format=None):
        """Checks if a users token is valid"""

        if request.user.is_authenticated:
            return Response(data={'detail': True},
                            headers= {'Content-Type': 'application/json'})


class HandleComponents(APIView):
    """Handle dockers or processes, reported by client"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    http_method_names = ('patch',)

    def patch(self, request, format=None):
        """Handle reports from client"""

        rq_values = request.data['values']

        if request.data['type'] == 'error':
            new_status = False
        elif request.data['type'] == 'running':
            new_status = True

        if rq_values['type'] == 'process':
            client_data = models.ClientProcesses.objects.get(
                                    id__exact=rq_values['id'])
            client_data.is_running = new_status

        elif rq_values['type'] == 'docker':

            client_data = models.ClientDockers.objects.get(
                                    id__exact=rq_values['id'])
            client_data.is_running = new_status

        client_data.save()

        return Response(data={'executen': 'done'},
                        headers= {'Content-Type': 'application/json'})


class UserProfileViewSet(viewsets.ModelViewSet):
    """Handle creating and updating profiles"""

    serializer_class = serializers.UserProfileSerializer
    queryset = models.UserProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.UpdateOwnProfile,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'email')

class UserLoginApiView(ObtainAuthToken):
    """Handle user authentication tokens"""

    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
