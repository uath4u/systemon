from django.contrib import admin
from rest_api import models

admin.site.register(models.UserProfile)
admin.site.register(models.ClientEndpoint)
admin.site.register(models.ClientProcesses)
admin.site.register(models.ClientDockers)
admin.site.register(models.EmailAddressToInform)
