from rest_framework import serializers
from rest_api import models


class ClientProcsModelSerializer(serializers.ModelSerializer):
    """Serializes the ClientProcesses model"""

    class Meta:
        model = models.ClientProcesses
        fields = ['id', 'process_to_check', 'email_to_inform']
        depth = 1


class ClientDockerModelSerializer(serializers.ModelSerializer):
    """Serializes the ClientDockers model"""
    class Meta:
        model = models.ClientDockers
        fields = ['id', 'docker_to_check', 'email_to_inform']
        depth = 1


class EmailAddressToInformSerializer(serializers.ModelSerializer):
    """Serializes the EmailAddressToInform model"""

    class Meta:
        model = models.EmailAddressToInform
        fields = ['email_to_inform']

class UserProfileSerializer(serializers.ModelSerializer):
    """Serializes the user profile object"""

    class Meta:
        model = models.UserProfile
        fields = ('id', 'email', 'name', 'password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {
                    'input_type': 'password'
                }
            }
        }

    def create(self, validated_data):
        """Create and return a new user"""

        user = models.UserProfile.objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password']
        )

        return user

    def update(self, instance, validated_data):
        """Handle update user account"""

        if password in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)

        return super().update(instance, validated_data)
