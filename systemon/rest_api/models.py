from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager


class UserProfileManager(BaseUserManager):
    """Manager for user profiles"""

    def create_user(self, email, name, password=None):
        """Create and save a new user profile"""

        if not email:
            raise ValueError('User must have an email address')

        email = self.normalize_email(email)
        user = self.model(email=email, name=name)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, name, password):
        """Create and save a superuse with given details"""

        user = self.create_user(email, name, password)
        user.is_superuser = True
        user.is_staff = True

        user.save(using=self._db)

        return user


class UserProfile(AbstractBaseUser, PermissionsMixin):
    """Database Model für user in their system"""

    class Meta:
        verbose_name = 'User Profile'

    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        """Retriev full name of user"""
        return self.name

    def get_short_name(self):
        """Retriev short name of user"""
        return self.name

    def __str__(self):
        """Return string representation of our user"""
        return self.email


class EmailAddressToInform(models.Model):
    """Model for Email addresses witch schould be informed if some error events
    happend"""

    email_to_inform = models.EmailField(max_length=255, unique=True)

    class Meta:
        verbose_name = 'Email addresses to inform by report'

    def __str__(self):
        """Return stringrepresentation for EmailAddressToInform"""

        return f'{self.email_to_inform}'


class ClientEndpoint(models.Model):
    """Database Model for IPs or Hostnames for a specific client"""

    ip_or_hostname = models.CharField(max_length=100, unique=True)

    def __str__(self):
        """Return string representation of our endpoint"""
        return self.ip_or_hostname


class ClientProcesses(models.Model):
    """Database Model for client processes to check from a specific client"""

    ip_or_hostname = models.ForeignKey(ClientEndpoint,
                                       on_delete=models.CASCADE)
    process_to_check = models.CharField(max_length=150)
    is_running = models.BooleanField(default=True)
    email_to_inform = models.ManyToManyField(EmailAddressToInform, related_name='email_list')

    class Meta:

        constraints = [models.UniqueConstraint(fields=['ip_or_hostname',
                                                       'process_to_check'],
                                               name='unique client processes')]
        verbose_name = 'Client Processe'

    def __str__(self):
        """Return string representation of the clientprocess"""
        return f'{self.ip_or_hostname}, {self.process_to_check}, {self.id}'


class ClientDockers(models.Model):
    """Database Model for client dockers to check form a specific client"""

    ip_or_hostname = models.ForeignKey(ClientEndpoint,
                                       on_delete=models.CASCADE)
    docker_to_check = models.CharField(max_length=150)
    is_running = models.BooleanField(default=True)
    email_to_inform = models.ManyToManyField(EmailAddressToInform)

    class Meta:

        constraints = [models.UniqueConstraint(fields=['ip_or_hostname',
                                                       'docker_to_check'],
                                               name='unique client docker')]
        verbose_name = 'Client Docker'

    def __str__(self):
        """Return string representation of the docker to check for"""
        return f'{self.ip_or_hostname}, {self.docker_to_check}, {self.id}'
